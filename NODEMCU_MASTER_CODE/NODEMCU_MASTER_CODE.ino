/*
 * Cria um servidor WiFi e um servidor WebSocket através dos quais é possível modificar o estado de um LED e notificar essas mesmas alterações. 
 * Através do servidor WiFi é possível alterar o estado do led para HIGH (/state/on) ou LOW (/state/off) e ainda aceder a uma página web onde se encontra um botão, 
 * sendo que quaisquer alterações ao estado desse botão são reproduzidas para o LED. A comunicação feita na página web é realizada através de websockets.
 * O servidor websocket é responsável por alterar o estado do LED sempre que a página web assim lhe comunicar e por notificar quaisquer outras páginas a ele conectado 
 * de que houve uma alteração de estado do LED e que, por isso, essa página se deve atualizar.
 */
#include <ESP8266WiFi.h>
#include <WebSocketsServer.h>
#include <Wire.h>

#define WIRE_SDA 4
#define WIRE_SCL 5

#define WS_PORT 81

/**
 * WI-FI Configuration
 */
const char* ssid = "VOID";
const char* password = "voidnowire";

/*
 * util string based on the websocket port
 */
const String ws_port_str = String(WS_PORT);

/**
 * Create wifi websocket server on specified port
 */
WebSocketsServer webSocket = WebSocketsServer(WS_PORT);

int lastStateReceived = 0;

void setup() {
  Serial.begin(115200);
  delay(10);
  
  WiFi.begin(ssid, password);
  WiFi.mode(WIFI_STA);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("WiFi connected to: ");
  Serial.println(ssid);
  /*
   * Start the wifi websocket server
   */
  webSocket.begin();
  webSocket.onEvent(webSocketEvent);
  Serial.println("WebSocketServer started");
  /* 
   * Print the IP address 
   */
  Serial.println(WiFi.localIP());

  Wire.begin(WIRE_SDA, WIRE_SCL);
}

void loop() {
  webSocket.loop();
  Wire.requestFrom(8,1);
   while (Wire.available()) { // slave may send less than requested
    int byteReceived = Wire.read();
    if((byteReceived >= 0
      && byteReceived < 150) /*&& lastStateReceived != byteReceived*/){
      Serial.println(byteReceived);
      String codeString = String(byteReceived);
      webSocket.broadcastTXT(codeString);//broadcast led change
     
    }
    lastStateReceived = byteReceived;
    Serial.println(byteReceived);
  }
}

/**
 * Processa qualquer evento associado ao servidor de websocket.
 * Os tipos de evento tidos em conta são:WStype_DISCONNECTED, WStype_CONNECTED, WStype_TEXT.
 * Quando uma página web se conecta ao servidor, a mensagem {"status":"Connected"} é enviada pelo canal de comunicação.
 * Quando uma página web envia um pedido ao servidor como texto, pelo websocket, o servidor verifica se o pedido corresponde a uma mudança no estado do LED e, se assim for, 
 * modifica o estado do led e notifica todas as outras páginas ligadas a si.
 */
void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t lenght) {
  switch(type) {
    case WStype_DISCONNECTED:
        Serial.printf("[%u] Disconnected!\n", num);
        break;
    case WStype_CONNECTED:
        {
            IPAddress ip = webSocket.remoteIP(num);
            Serial.printf("[%u] Connected from %d.%d.%d.%d url: %s\n", num, ip[0], ip[1], ip[2], ip[3], payload);
            // send message to client
            webSocket.sendTXT(num, "{\"status\":\"Connected\"}");
        }
        break;
    case WStype_TEXT:
      break;
    case WStype_BIN:
      break;
  }
}
