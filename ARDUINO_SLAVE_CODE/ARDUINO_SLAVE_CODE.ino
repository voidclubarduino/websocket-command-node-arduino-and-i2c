 #include <Wire.h>

  /*
   * L = 1
   * R = 2
   * U = 4
   * D = 8
   * PAUSE = 13
   */
int const L_ = 2;   
int const R_ = 1;   
int const U_ = 8;   
int const D_ = 4;   
int const PAUSE_ = 16;
int const I2C_ADDR = 8;   
#define X  1 // analog pin connected to X output
#define Y  0 // analog pin connected to Y output
#define PAUSE  2 // analog pin connected to SWITCH
#define B_PIN 11
#define A_PIN 10

int val = 0;
int lastVal = 0;
bool stopped = false;


 /**
 * Variáveis necessárias para debouncing.
 */
int buttonStateB;      
int lastButtonStateB = LOW;          
int buttonStateA;             
int lastButtonStateA = LOW;          
long lastDebounceTime = 0;
/**
 * DEBOUNCE DELAY
 */
long debounceDelay = 50;


void setup() {
  Serial.begin(9600);
  //I2C
  Wire.begin(I2C_ADDR);
  Wire.onRequest(requestEvent);
  pinMode(B_PIN,INPUT);
  pinMode(A_PIN,INPUT);
  pinMode(X,INPUT);
  pinMode(Y,INPUT);
  pinMode(PAUSE,INPUT);
  digitalWrite(PAUSE,HIGH);
}
 
void loop() {
  val = 0;
  /**
   *  X and Y axis
   * 
   */
  int x = analogRead(X);
  int y = analogRead(Y);
  int v1 =   processRead(map(x, 0, 1023, 0, 255), L_, R_);
  int v2 =   processRead(map(y, 0, 1023, 0, 255), U_, D_); 
  int pause  = digitalRead(PAUSE);
  if(pause == 0){
    val = PAUSE_;
    stopped = true;
  }else{
    if(stopped){
      val = lastVal;
      stopped = false;
    }else{
      val= v1|v2;
    }
  }

  Serial.println();
  val |= digitalRead(B_PIN) == 1 ? 64 : 0;
  val |= digitalRead(A_PIN) == 1 ? 32 : 0;

}
int processRead(int dataRead, int state1, int state2){
  int noise = 2;
  if(dataRead < (128 - noise)){
    return state1;
  }else if(dataRead > (128 + noise)){
    return state2;
  }else{
    return 0;
  }
}
//I2C REQUEST EVENT
void requestEvent() {
  Wire.write(val);
}
boolean processDebounce(int pin, int buttonState, int lastButtonState){
  // ler o estado do pino para uma variável local
  int sensorVal = digitalRead(pin);
  /*
   * verificar se o botão foi pressionado e o tempo que decorreu desde o último pressionar do botão 
   * é suficiente para ignorar qualquer tipo de ruído.
  */

  /*
   * Se a leitura registou uma alteração de estado, seja ele ruído ou o pressionar do botão
  */
  if (sensorVal != lastButtonState) {
    // reiniciar o contador de debouncing
    lastDebounceTime = millis();
  }
  if ((millis() - lastDebounceTime) > debounceDelay) {
    /**
     * Qualquer que seja a leitura, esta aconteceu a um tempo superior ao intervalo 
     * de debouncing considerado (no exemplo 50 milisegundos).
     * Por essa razão, pode se assumir a leitura como sendo o estado atual do botão.
     */
     buttonState = sensorVal;
    return true;
  }
  /*
   * definir último estado lido, para o botão, como sendo a leitura atual.
   */
  lastButtonState = sensorVal;
  return false;
}
